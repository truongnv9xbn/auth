package ai.yunlove.packages.auth.cas;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResult {
    private String id;
    private String name;
    private String username;
    private String phone;
    private String email;
    private short type;
}
