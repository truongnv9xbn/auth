package ai.yunlove.packages.auth.cas;

import ai.yunlove.packages.auth.AuthenticationFilter;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;

public class CasAuthenticationFilter extends AuthenticationFilter {
    static String BEARER = "Bearer";
    public CasAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected String getCredentials(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (!StringUtils.startsWith(token, BEARER)) {
            return null;
        }
        token = StringUtils.removeStart(token, BEARER + " ");
        return token;
    }

}
