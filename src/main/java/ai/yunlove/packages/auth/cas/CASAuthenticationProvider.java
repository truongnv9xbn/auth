package ai.yunlove.packages.auth.cas;

import ai.yunlove.packages.auth.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RequiredArgsConstructor
public class CASAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private final RestTemplate restTemplate;

    @Value("${cas.authenticationEndpoint:null}")
    private String authenticationEndpoint;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        try {
            String token = String.valueOf(authentication.getCredentials());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setBearerAuth(token);

            HttpEntity<String> httpRequest = new HttpEntity<>(null, headers);
            ResponseEntity<? extends AuthResult> response = this.restTemplate.exchange(authenticationEndpoint, HttpMethod.GET, httpRequest, getAuthResultClass());

            AuthResult authResult = response.getBody();
            if (null != authResult && StringUtils.isNotEmpty(authResult.getId())) {
                return this.mapToUserDetails(authResult);
            }
        } catch (Exception exception) {
            log.warn(exception.getMessage());
        }

        throw new InternalAuthenticationServiceException(this.getClass().getName() + " returned null");

    }

    protected UserDetails mapToUserDetails(AuthResult authResult) {
        return User.builder().id(
                        authResult.getId())
                .name(authResult.getName())
                .email(authResult.getEmail())
                .username(authResult.getUsername())
                .phone(authResult.getPhone())
                .type(authResult.getType())
                .build();
    }

    protected Class<? extends AuthResult> getAuthResultClass() {
        return AuthResult.class;
    }


}
