package ai.yunlove.packages.auth;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Auth {
    public static User user() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        return null;
    }

    public static String getId() {
        return user() != null ? Objects.requireNonNull(user()).getId() : null;
    }

    public static String getUserName() {
        return user() != null ? Objects.requireNonNull(user()).getUsername() : null;
    }

}
