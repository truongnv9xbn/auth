package ai.yunlove.packages.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
public class User implements UserDetails {
    public static short TYPE_SERVICE = 1;
    public static short TYPE_USER = 2;

    protected String id;
    protected String name;
    protected String username;
    protected String phone;
    protected String email;
    protected short type = TYPE_USER;

    protected LocalDateTime createdAt;

    protected LocalDateTime updatedAt;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
