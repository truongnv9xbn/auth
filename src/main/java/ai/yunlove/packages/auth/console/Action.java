package ai.yunlove.packages.auth.console;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Action {
    private String code;
    private List<String> endpoints;
}
