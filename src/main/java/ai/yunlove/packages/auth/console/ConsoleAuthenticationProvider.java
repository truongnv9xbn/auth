package ai.yunlove.packages.auth.console;

import ai.yunlove.packages.auth.cas.CASAuthenticationProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
@Component
public class ConsoleAuthenticationProvider extends CASAuthenticationProvider {

    public ConsoleAuthenticationProvider(RestTemplate restTemplate) {
        super(restTemplate);
    }

    @Override
    protected UserDetails mapToUserDetails(ai.yunlove.packages.auth.cas.AuthResult authResult) {

        AuthResult csAuthResult = (AuthResult) authResult;

        return User.builder()
                .id(csAuthResult.getId())
                .name(csAuthResult.getName())
                .email(csAuthResult.getEmail())
                .createdAt(LocalDateTime.ofInstant(Instant.ofEpochSecond(csAuthResult.getCreatedAt()), ZoneId.systemDefault()))
                .accessRights(csAuthResult.getAccessRights())
                .build();

    }

    @Override
    protected Class<? extends ai.yunlove.packages.auth.cas.AuthResult> getAuthResultClass() {
        return AuthResult.class;
    }
}
