package ai.yunlove.packages.auth.console;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Hidden {
    private String object;
    private List<String> attributes;
}
