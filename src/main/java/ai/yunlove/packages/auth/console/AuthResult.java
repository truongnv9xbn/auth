package ai.yunlove.packages.auth.console;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AuthResult extends ai.yunlove.packages.auth.cas.AuthResult {
    @JsonProperty("access_rights")
    private List<AccessRight> accessRights;
    @JsonProperty("created_at")
    private Long createdAt;
}
