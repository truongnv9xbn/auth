package ai.yunlove.packages.auth.console;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessRight implements GrantedAuthority {
    private String right;
    private String condition;
    private List<Hidden> hidden;
    private Object invisible;

    @Override
    public String getAuthority() {
        return right;
    }
}
