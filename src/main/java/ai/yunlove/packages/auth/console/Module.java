package ai.yunlove.packages.auth.console;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Module {
    private String code;
    private String name;
    private List<Action> actions;
}
