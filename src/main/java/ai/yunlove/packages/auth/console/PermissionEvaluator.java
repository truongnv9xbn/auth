package ai.yunlove.packages.auth.console;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Slf4j
@Component
public class PermissionEvaluator implements org.springframework.security.access.PermissionEvaluator {
    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {
        return this.hasPermission(auth, null, permission);
    }

    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {
        try {
            if ((auth == null) || !(permission instanceof String)) {
                return false;
            }

            User grantedAuth = (User) auth.getPrincipal();
            return grantedAuth.can(permission.toString());
        } catch (Exception ex) {
            log.warn(ex.getMessage());
        }

        return false;
    }

}
