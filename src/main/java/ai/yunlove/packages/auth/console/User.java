package ai.yunlove.packages.auth.console;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@SuperBuilder
public class User extends ai.yunlove.packages.auth.User {

    @JsonIgnore
    private List<AccessRight> accessRights;

    public boolean can(String permission) {
        if (this.getAccessRights().isEmpty()) {
            return false;
        }
        return this.getAccessRights().stream().anyMatch(accessRight -> permission.equals(accessRight.getRight().toLowerCase()));
    }

    public List<Hidden> getHidden(String permission) {
        if (this.getAccessRights().isEmpty()) {
            return null;
        }
        Optional<List<Hidden>> result = this.getAccessRights().stream().filter(e -> permission.equals(e.getRight().toLowerCase())).map(AccessRight::getHidden).findAny();
        return result.orElse(null);
    }
}
