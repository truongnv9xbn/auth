package ai.yunlove.packages.auth.console;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
@RequiredArgsConstructor
@Slf4j
public class DefaultPermissionRepository implements PermissionRepository {
    private final static String RESOURCE_FILE_NAME = "permission.json";

    @Override
    public Object discovery() {
        InputStream inputStream = getClass()
                .getClassLoader()
                .getResourceAsStream(RESOURCE_FILE_NAME);

        if (inputStream != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                return objectMapper.readValue(inputStream, Object.class);
            } catch (IOException e) {
                log.warn(e.getMessage());
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.warn(e.getMessage());
                }
            }
        } else {
            log.info("Không thể tìm thấy file {}", RESOURCE_FILE_NAME);
        }
        return null;
    }
}
