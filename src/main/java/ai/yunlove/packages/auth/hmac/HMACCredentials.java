package ai.yunlove.packages.auth.hmac;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class HMACCredentials {
    private String id;
    private String username;

}
