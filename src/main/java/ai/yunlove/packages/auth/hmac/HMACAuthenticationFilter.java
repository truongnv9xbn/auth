package ai.yunlove.packages.auth.hmac;

import ai.yunlove.packages.auth.AuthenticationFilter;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;

public class HMACAuthenticationFilter extends AuthenticationFilter {

    public HMACAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    protected Object getCredentials(HttpServletRequest request) {
        String consumerId = request.getHeader("X-Consumer-Custom-Id");
        String credentialIdentifier = request.getHeader("X-Credential-Identifier");
        if (StringUtils.isEmpty(consumerId) || StringUtils.isEmpty(credentialIdentifier)) {
            return null;
        }
        return new HMACCredentials(consumerId, credentialIdentifier);
    }
}
