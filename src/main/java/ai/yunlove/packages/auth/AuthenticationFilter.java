package ai.yunlove.packages.auth;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;

public abstract class AuthenticationFilter extends OncePerRequestFilter {

    protected final AuthenticationManager authenticationManager;

    public AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Authentication authenticationResult = attemptAuthentication(request);
        if (authenticationResult != null) {
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
        }

        filterChain.doFilter(request, response);
    }

    public Authentication attemptAuthentication(HttpServletRequest request) throws AuthenticationException {

        Authentication requestAuthentication = new UsernamePasswordAuthenticationToken(null, this.getCredentials(request));

        return authenticationManager.authenticate(requestAuthentication);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return Objects.isNull(this.getCredentials(request));
    }

    protected abstract Object getCredentials(HttpServletRequest request);

}
